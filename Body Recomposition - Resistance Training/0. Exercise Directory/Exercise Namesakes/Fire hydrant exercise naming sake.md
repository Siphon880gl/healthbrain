The fire hydrant exercise is a bodyweight exercise that targets the glutes, hips, and core muscles. And yeah: You'll be mimicking how a dog might behave at a fire hydrant.

![](GAhCeLX.png)

^ Pic from https://www.hingehealth.com/resources/articles/fire-hydrants/