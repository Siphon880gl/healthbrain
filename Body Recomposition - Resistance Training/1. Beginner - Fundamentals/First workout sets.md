You're trying to predict the appropriate weight for the first sets of your exercises. You are starting a new workout program or your old stats are no longer relevant because it's been too long ago

Predict weights:
Standard levels for your exercise https://strengthlevel.com/strength-standards
Adjust to the rep range: https://www.allthingsgym.com/rep-max-calculator/


Or guess at weight and see how many weights you can do till failure -> Calculate the 1 rep max
Keep in mind the lower the reps to failure, the more accurate the 1 rep max calculator will be
https://www.nasm.org/resources/one-rep-max-calculator


| **Training Goal** | **Percent of 1RM** | **Repetitions per Set** | **Sets per Exercise** | **Rest Periods** |
| ----------------- | ------------------ | ----------------------- | --------------------- | ---------------- |
| **Endurance**     | 50-70%             | 12-20                   | 2-3                   | 30-60 seconds    |
| **Hypertrophy**   | 65-85%             | 6-12                    | 3-4                   | 60-90 seconds    |
| **Strength**      | 85-100%            | 1-6                     | 3-6                   | 2-5 minutes      |

---


**Over-/Under-guessed starting workout, or weight doesn’t exist, or time to increase weight:**

Change weight to hit new rep range or to progress or to make easier:

| 1 rep -/+  | 2 reps -/+ | 3 reps -/+ | 4 reps -/+ |
| ---------- | ---------- | ---------- | ---------- |
| 2.5%       | 5%         | 7.5%       | 10%        |
| ✖️1.025    | ✖️ 1.05    | ✖️ 1.075   | ✖️ 1.1     |
| ✖️(1-.025) | ✖️(1-.05)  | ✖️(1-.075) | ✖️.9       |

If weight not available, add tempo:

| Tempo (Seconds) | Estimated Increase in Difficulty (%) |
| --------------- | ------------------------------------ |
| 1               | 5-10%                                |
| 2               | 10-20%                               |
| 3               | 20-30%                               |
| 4               | 30-50%                               |

^ Remembering that:

| 1 rep -/+  | 2 reps -/+ | 3 reps -/+ | 4 reps -/+ |
| ---------- | ---------- | ---------- | ---------- |
| 2.5%       | 5%         | 7.5%       | 10%        |

You may add microplates: 0.25, 0.5, 0.75, 1, 1.25