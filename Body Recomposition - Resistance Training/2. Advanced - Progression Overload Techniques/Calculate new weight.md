
To lower the number of reps at same rpe intensity to target strength or hypertrophy
Last row is just for completeness

| 1 rep -/+  | 2 reps -/+ | 3 reps -/+ | 4 reps -/+ |
| ---------- | ---------- | ---------- | ---------- |
| 2.5%       | 5%         | 7.5%       | 10%        |
| ✖️1.025    | ✖️ 1.05    | ✖️ 1.075   | ✖️ 1.1     |
| ✖️(1-.025) | ✖️(1-.05)  | ✖️(1-.075) | ✖️.9       |
