
A study found that lifting with a faster tempo on the concentric portion (1 second vs. 3 seconds) was better for muscle gain. With respect to the eccentric portion of a repetition, there is evidence to suggest that **slower (4 seconds vs.** **1 second) is better**.

https://examine.com/conditions/muscle-size-and-strength/faq/whats-the-best-lifting-tempo/#:~:text=Another%20study%20found%20that%20lifting,1%20second)%20is%20better.