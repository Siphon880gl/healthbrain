
Adding tempo on the eccentric range (more likely muscle breakdown for hypertrophy)

|   |   |
|---|---|
|Tempo (Seconds)|Estimated Increase in Difficulty (%)|
|1|5-10%|
|2|10-20%|
|3|20-30%|
|4|30-50%|


 Remembering that:

| 1 rep -/+  | 2 reps -/+ | 3 reps -/+ | 4 reps -/+ |
| ---------- | ---------- | ---------- | ---------- |
| 2.5%       | 5%         | 7.5%       | 10%        |
