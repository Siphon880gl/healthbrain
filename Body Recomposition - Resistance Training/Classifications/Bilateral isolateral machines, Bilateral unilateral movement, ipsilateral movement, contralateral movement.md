
Isolateral machines are unilateral machines, which means they use one limb or side of the body to produce force. Bilateral machines are used to train both sides of the body simultaneously. There could be isolateral machine shoulder press where you push the weight simultaneously in a bilateral movement.

Bilateral movement means both limbs go same time. Eg. Bilateral bicep curls, without alternation. Unilateral movement means you perform on one limb or one side, either throughout the set or it's unilateral because you're performing alternating movements (eg. alternating bicep curls).

Unilateral movements are broken down into contralateral or ipsilateral movements.
- Contralateral. Muscles involved on opposite sides. Eg. one leg / one arm shoulder press, where leg stands on opposite side
	- Hits all three planes of movements (sag, frontal, transverse). The opposing side where the leg is, is resisting falling over on the frontal and transverse planes.
	- External oblique on the side of the arm. Internal oblique on the standing leg.
- Ipsilateral. Muscles involved on same side. Eg. one leg / one arm shoulder press, where leg stands on the same side as weight
	- Still hits all three planes of movements but more intense on the sag plane on that active side.


Source: https://www.youtube.com/watch?v=POfXmX2uP2w