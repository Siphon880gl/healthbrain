
Filtered tap water in a reusable glass or stainless steel bottle is a more sustainable, healthy, and cost-effective choice compared to bottled water. Exceptions might include emergency situations where tap water is unsafe to drink. Here we highlight five reasons to choose filtered tap water over bottled water,:

1. **Cost-Effectiveness**: Tap water costs significantly less than bottled water—about two-tenths of a penny per gallon compared to approximately a dollar per liter at convenience stores. This makes tap water about 2,000 times cheaper than bottled water.
    
2. **Quality and Transparency**: Tests have shown that bottled water can contain various contaminants like industrial chemicals and bacteria. Additionally, bottled water companies are not required to disclose their water testing results, unlike municipal tap water providers who must test and publish their findings annually.
    
3. **Health Concerns with Plastic Bottles**: Plastic bottles, especially those made from PET, may leach harmful chemicals into the water. These bottles can contain numerous chemical additives and impurities that could pose health risks.
    
4. **Environmental Impact**: Only about 30% of PET plastic bottles are recycled, leaving the majority to contribute to landfill waste and environmental pollution. This includes large amounts of plastics ending up in oceans, forming massive floating garbage patches.
    
5. **Energy Consumption**: Producing bottled water requires up to 2,000 times more energy than tap water, considering the processes of manufacturing bottles, filling them, and transporting them, often over long distances.
    


Detailed:
[https://www.ewg.org/tapwater/bottled-water-resources.php](https://www.ewg.org/tapwater/bottled-water-resources.php)