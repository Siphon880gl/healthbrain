**2300mg**  
The American Heart Association recommends _no more than 2,300 mg a day_ and moving toward an ideal limit of no more than 1,500 mg per day for most   

---

**For Comparison**

Car Jr Double Western Bacon Cheeseburger already at 2050mg sodium!
[https://www.carlsjr.com/getmedia/fbb8b09a-2dc5-4cac-b73e-643fd7ab6d33/usnutritionvaluescarlsjr.pdf](https://www.carlsjr.com/getmedia/fbb8b09a-2dc5-4cac-b73e-643fd7ab6d33/usnutritionvaluescarlsjr.pdf)