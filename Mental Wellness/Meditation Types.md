- **Mindful breathing** This practice involves paying attention to the sensation of the breath as it moves in and out of the body. Simply observe the breath without trying to control it.

- **Body scan** Lie down and bring your attention to each part of your body, starting at the toes and moving up to the top of your head. Notice any sensations or feelings in each area without judging or trying to change them.

- **Mindful walking** This practice involves walking slowly and intentionally, paying attention to the sensations of each step and the movement of your body.

- **Loving-kindness meditation** This practice involves cultivating feelings of love and kindness towards yourself and others. Repeat phrases such as "may I be happy, may I be healthy, may I be safe" or "may you be happy, may you be healthy, may you be safe.”

- **Mindful eating** This practice involves paying attention to the taste, texture, and sensation of food as you eat. Notice any thoughts or feelings that arise without judging or reacting to them.

- **Mindful listening** This practice involves giving your full attention to the sounds around you, without judging or reacting to them. Simply listen and notice.

- **Gratitude meditation** This practice involves reflecting on the things in your life that you are grateful for. Focus on the feeling of gratitude and the positive emotions it generates.

- **Mindful communication** This practice involves paying attention to the words you use, the tone of your voice, and your body language when communicating with others. Be present in the moment and listen deeply.

- **Mindful self-compassion** This practice involves treating yourself with kindness, compassion, and understanding. Notice any negative thoughts or feelings that arise and respond with self-compassion.

- **Mindful relaxation** This practice involves consciously relaxing your body and mind. Find a comfortable position and focus on releasing tension from each part of your body, one at a time.

- **Mindful observation** This practice involves observing your thoughts, feelings, and sensations without judgment. Simply notice them as they arise and let them pass without getting caught up in them.

- **Mindful visualization** This practice involves visualizing a peaceful scene or place and immersing yourself in the experience. Notice the sights, sounds, and sensations of the scene as vividly as possible.

- **Mindful stretching** This practice involves stretching your body in a slow and intentional way, paying attention to the sensations in each muscle and joint.

- **Mindful time in nature** This practice involves spending time in nature and being fully present to the sights, sounds, and sensations around you. Engage your senses and be fully immersed in the experience.