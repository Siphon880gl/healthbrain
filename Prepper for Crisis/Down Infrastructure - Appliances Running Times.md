
## **Appliances Running Time**
- Portable radio (5W) = 100H
- Flashlight (3W) = 166.7H
- Portable fan (30W) = 33.3H
- Power bank (10W) = 50H
- Mobile Phone (29W) = 37.0H
- Laptop (70W) = 15.3H
- CPAP Machine (60W) = 17.9H

---


## Cooking
Electric Grill (850W) = 1.2H
Portable Induction Hob: 800-3,000W
Battery-powered versions typically last 2-3 hours on a single charge
Portable Electric Kettle: 1,000-1,500W
Most battery-powered kettles can boil water 3-5 times before needing a recharge
Portable Rice Cooker: 200-500W
Battery-operated models can usually cook 1-2 batches of rice on a single charge
Portable Pressure Cooker: 700W
Battery-powered versions typically last for 1-2 cooking cycles
Portable Microwave: 600-1,500W
Battery-operated models are rare, but some can run for about 30-45 minutes on a single charge
Portable Sandwich Maker: 700-1,000W
Battery-powered versions can usually make 4-6 sandwiches before needing a recharge
Portable Single-Serve Brewer: 900-1,500W
Battery-operated models can typically brew 3-5 cups of coffee on a single charge


----

## Jackery Solar Generator 3000 Pro

Specs:
- Portable
- Weight ~60lb
- $2.5K-3K

![](uxd4k6W.png)
^ Above from https://www.jackery.com/products/explorer-3000-pro-portable-power-station?gad_source=1&gclid=Cj0KCQiAyoi8BhDvARIsAO_CDsBf64F-KZGFFhH7k0GvxRhOju9Jgyo_K9P25V6Qzn3Fw6zY42_4cOgaAh3OEALw_wcB

The Jackery Solar Generator 3000 Pro is a powerful, versatile portable power solution designed for both outdoor adventures and home emergency preparedness. With its impressive **3024Wh capacity** and **3000W output** (6000W surge peak), this solar generator can power up to **99% of household appliances**[1][4][7].

Key Features:
- **High Capacity**: Supports most home and outdoor electrical needs
- **Multiple Charging Options**: 
  - Wall charging in just 2.4 hours
  - Solar charging with 6 SolarSaga panels in 3-4 hours
- **Portable Design**: Lightweight at 63.93 lbs with wheels and foldable handle
- **Temperature Versatile**: Operates in temperatures from -20°C to 40°C

Ideal for:
- RV trips
- Home power outages
- Camping and outdoor activities
- Emergency backup power

Practical Applications:
- Can run a refrigerator for 5.1 hours
- Powers coffee makers, AC units, and induction stoves
- Supports medical equipment and communication devices
- Provides reliable power for essential electronics[7]

With its robust design and flexible charging capabilities, the Jackery Solar Generator 3000 Pro offers a reliable electrical lifeline during unexpected situations.

Citations:
[1] https://powermy.com/large-power-stations-over-1600wh/jackery-explorer-3000-pro-portable-power-station/
[2] https://us.ecoflow.com/pages/whole-home-generator
[3] https://growattportable.com/pages/solar-generators
[4] https://portablepowerplus.com/products/jackery-3000-pro-200w-solar-panel
[5] https://poweredportablesolar.com/ultracap/
[6] https://goalzero.com/collections/portable-solar-generator-kits
[7] https://www.jackery.com/blogs/buying-advice/what-can-jackery-3000-pro
[8] https://www.reddit.com/r/solar/comments/164feh4/solar_batteries_or_propane_generator_for_backup/
[9] https://www.jackery.com/pages/rv-generators