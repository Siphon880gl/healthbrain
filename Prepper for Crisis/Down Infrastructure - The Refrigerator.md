During a power outage, your refrigerator can maintain cold temperatures for a limited time if you follow these critical guidelines:

- A **refrigerator** will keep food cold for **about 4 hours** if it remains **unopened**[1][3][7]
- A **full freezer** will maintain its temperature for approximately **48 hours** (24 hours if half full) if the door remains closed[1][3]

Key Tips to Preserve Food Safety:

1. **Keep Doors Closed**: Limit opening refrigerator and freezer doors to maintain cold temperature[1][3][5][7]

2. **Use Thermometers**: 
   - Refrigerator should stay at or below 40°F (4°C)
   - Freezer should remain at or below 0°F (-18°C)[3]

3. **Preparation Strategies**:
   - Freeze water containers or ice packs in advance
   - Group refrigerated and frozen foods together
   - Consider using dry ice for prolonged outages[1][5]

4. **After Power Returns**:
   - Check food temperatures
   - Discard perishables left at room temperature for over 2 hours
   - When in doubt, throw it out to prevent foodborne illness[1]

Remember, the key is minimizing door openings and monitoring temperature to keep your food safe during a power outage.

Citations:
[1] https://www.homefoodservices.com/post/food-safety-in-a-power-outage
[2] https://www.temparmour.com/backup-power
[3] https://www.fda.gov/food/buy-store-serve-safe-food/food-and-water-safety-during-power-outages-and-floods
[4] https://www.mediproducts.net/solutions/cold-storage-refrigeration/battery-backup-for-medical-refrigerator
[5] https://www.chesco.org/2623/Restaurants---how-to-respond-to-a-power-
[6] https://www.bluettipower.com/blogs/news/5-best-battery-backup-for-refrigerators-during-power-outages-2023
[7] https://community.fema.gov/ProtectiveActions/s/article/Power-Outage-Keep-Freezers-and-Refrigerators-Closed-as-Much-as-Possible
[8] https://www.jackery.com/blogs/buying-advice/battery-backup-for-refrigerator