
```toc
```

Recommended: Use table of contents at the top right

---
## Stay in the Know

### Emergency Messages
- Your county will text you with emergency tone. It could text you a warning or an immediate evacuation order.
- Your city's water department website or city website will announce if water is unsafe to drink.

### Digital Tools
Fire Maps

- Watch Duty app [https://www.watchduty.org/](https://www.watchduty.org/)
- Fire.ca.gov (Also shows containment %) [https://www.fire.ca.gov/](https://www.fire.ca.gov/)
- NASA Firms [https://firms.modaps.eosdis.nasa.gov/usfs/map/#t:tsd;d:24hrs;@-118.07,34.19,13.29z](https://firms.modaps.eosdis.nasa.gov/usfs/map/#t:tsd;d:24hrs;@-118.07,34.19,13.29z)

Containment %

- Fire.ca.gov (Also shows containment %) [https://www.fire.ca.gov/](https://www.fire.ca.gov/)

Air Quality 

- Air Now [https://www.airnow.gov/?city=Pasadena&state=CA&country=USA](https://www.airnow.gov/?city=Pasadena&state=CA&country=USA)
- Plume Labs [https://air.plumelabs.com/air-quality-in-Pasadena-ca-mzWI](https://air.plumelabs.com/air-quality-in-Pasadena-ca-mzWI)

Online Communities
- Join IG and FB official groups of the affected communities for important news.
- Join IG and FB unofficial groups of the affected communities for on-the-ground updates from residents.


---

## Sources Receive/Provide

### Possible aid types

You can receive or provide:

- Animal Boarding
- Distribution Hub
- Donation Drives
- Food
- Free Items
- Free Prepared Meals
- Fundraiser
- Horse Hauling
- Information
- Medical
- Services
- Shelter
- Transportation
- Wifi/Charging
- Workspace
### Receive/Provide - Directory of Fire and Wind Resources by Mutual Aid LA Network 2025
[https://docs.google.com/spreadsheets/d/1KMk34XY5dsvVJjAoD2mQUVHYU_Ib6COz6jcGH5uJWDY/htmlview?fbclid=IwZXh0bgNhZW0CMTEAAR1z1G-5osJXRLcNrhD2pOirixgc4XHtW3G2IwG48CBRd4MfHQkTAOlzfyw_aem_kgMMrgPYtgMTovlZ5-0TfA](https://docs.google.com/spreadsheets/d/1KMk34XY5dsvVJjAoD2mQUVHYU_Ib6COz6jcGH5uJWDY/htmlview?fbclid=IwZXh0bgNhZW0CMTEAAR1z1G-5osJXRLcNrhD2pOirixgc4XHtW3G2IwG48CBRd4MfHQkTAOlzfyw_aem_kgMMrgPYtgMTovlZ5-0TfA)

### Provide - PCC Pasadena City College is asking for spare:
Visit Facebook group "Pasadena Now" or Pasadena City College for exact times and location
https://www.facebook.com/pasadenacitycollege
﻿﻿
- ﻿﻿Nonperishable food
- Water
- ﻿OTC medicine
- ﻿Chapstick
- ﻿﻿Allergy medicine
- ﻿﻿Any hygiene products
- ﻿﻿Baby formula
- ﻿﻿Diapers
- ﻿Clothes
- ﻿﻿Blankets and towels
- ﻿﻿Flashlights with batteries
- ﻿Portable chargers

### Receive/Provide Ideas
- PCC list
- air purifiers
- gloves
- mask
- 
### Provide Firefighters
- Water
- Proteins
- No sugary snack that will crash

### Receive from Brands
- If the disaster is nation wide news, some brands may provide resources. You'll often see their social media posts offering help / stay / food / products on IG.

### Receive/Provide per City
- Join IG and FB official groups of the affected communities for receive/provide opportunities.

### Receive from Government: FEMA Assistance
- If the disaster is nation wide news:
![](BAA9haz.png)


### Receive/Provide from Government: Red Cross
- If the disaster is nation wide news, join IG and FB Red Cross groups for receiving/providing opportunities.

---

## Evacuation SOP 1
**EMERGENCY EVACUATION – ITEMS TO GATHER**  
No. of vehicles: ______ Drivers: ______ Trailers: ______ Motorcycles: ______

### 15-minute warning:

1. Wallet, purse, keys, glasses
2. Cell phone(s), charger(s)
3. Emergency cash, credit card
4. Pets, carriers, leashes, meds
5. Clothes, shoes, hats for the season
6. Hearing aids, medications
7. Flashlights, extra batteries
8. Safety deposit box key(s)
9. Checkbooks, bills to pay   

### 30-minute warning:

(_Includes the above, plus:_)

1. Pillows, sleeping bags, blankets
2. Address book, phone list
3. Jewelry & most-valuable personal possessions
4. Personal hygiene items
5. Other meds, supplements
6. First aid kit, medical items
7. Pet food, dishes, bedding, litter
8. Children’s items, toys, books
9. Battery radio, extra batteries
10. Toilet paper, hand wipes, soap
11. Clothing for 3 days, shoes
12. Computer, monitor, laptop
13. Gallons of drinking water
### 1-hour warning:

1. Take or safeguard guns, ammo
2. Ice cooler with ice, food, drinks
3. Genealogy records, files
4. 3 days’ food, special diet items
5. Gloves, dust mask for smoke
6. Paper plates, cups, utensils
7. School items (homework, pen, pencil, books, calculator, paper)
8. Licenses, vehicle titles, deeds
9. Insurance, financial, medical data; Wills, Powers of Attorney
10. Personal property list, photos & appraisals, documentation
### 2-hour+ warning:

1. Albums, photos, home videos
2. Family photos on display
3. Military decorations, records, mementos, plaques
4. Luggage (packed)
5. Valuable items, cameras
6. Heirlooms, art, collections
7. Primary cosmetics
8. Secondary vehicles, RV
9. Camping equipment, tent
10. Journals, diaries, letters

---

## Evacuation SOP 2

|**Stored**|**Scanned**|**Cloud**|**Documents**|
|---|---|---|---|
||||Birth Certificates|
||||Passports/Visas|
||||Home insurance documents|
||||Car insurance documents and registration|
||||Health insurance documents/Medical Cards/Medical Records/glass prescriptions|
||||Employment records|
||||Tax returns|
||||Drivers licenses|
||||Social Security Card|
||||Back-up ID (student ID, military, DD214, VA, etc.)|
||||Credit Cards|
||||Medical history|
||||Power of attorney|
||||Wills|
||||Concealed carry license|
||||Important phone numbers|
||||Titles/Deeds/etc.|
||||Marriage License|
||||At least one statement from all financial accounts with account #, phone number, address, etc.|
||||All military and VA records|
||||Thumb drive with video of house and all contents|
||||Video of everything in house, all rooms, all drawers open, all closet contents, basement, garage, attic, including artwork|
||||Vet records for your pets, including rabies and other information; would be needed for boarding|
