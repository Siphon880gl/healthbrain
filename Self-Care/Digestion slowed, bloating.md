#### Hard vs easy to digest
##### **Foods to Avoid:**

❌ **Greasy & Fried Foods** – Hard to digest and may worsen bloating.  
❌ **Dairy Products** – Some people experience increased mucus production and bloating.  
❌ **Carbonated Drinks** – Can cause gas and bloating.  
❌ **Cruciferous Vegetables (Broccoli, Cauliflower, Cabbage, Brussels Sprouts)** – Can lead to gas and discomfort.  
❌ **Legumes (Beans, Lentils, Chickpeas, etc.)** – May cause bloating due to fermentation in the gut.  
❌ **Processed & Sugary Foods** – Can slow digestion.

##### **Foods to Eat Instead:**

✅ **Warm Soups & Broths** – Easy on digestion and provide hydration.  
✅ **Ginger Tea** – Helps with bloating and aids digestion.  
✅ **Bananas & Apples** – Gentle on the stomach and provide energy.  
✅ **Plain Rice or Toast** – Light and easy to digest.  
✅ **Steamed Vegetables (Carrots, Zucchini, Squash, etc.)** – Less likely to cause bloating.  
✅ **Probiotic-Rich Foods (Yogurt, Kefir, Kimchi, Sauerkraut, etc.)** – Support gut health.

Avoiding greasy, heavy, and bloating foods can help ease digestion.