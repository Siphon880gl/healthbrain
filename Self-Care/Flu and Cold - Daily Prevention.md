## **Daily Prevention Against Cold & Flu**

### **Lifestyle Habits**

- Wash hands frequently
- Avoid touching face (eyes, nose, mouth)
- Get enough sleep (7-9 hours)
- Stay hydrated (water, herbal teas)
- Exercise regularly
- Manage stress (meditation, deep breathing)

### **Diet & Nutrition**

- **Vitamin C** (citrus fruits, bell peppers, kiwi)
- **Vitamin D** (sunlight, fatty fish, fortified dairy)
- **Zinc** (nuts, seeds, beans, lean meats)
- **Probiotics** (yogurt, kefir, sauerkraut)
- **Garlic & Ginger** (natural immune boosters)
- **Turmeric** (anti-inflammatory properties)
- **Green Tea** (antioxidants & antiviral effects)

### **Supplements (If Needed)**

- **Vitamin C** (1000mg daily during flu season)
- **Vitamin D** (especially in winter)
- **Zinc** (helps shorten colds if taken early)
- **Elderberry** (supports immunity & may reduce symptoms)


### On first day feel a little off
- Zicam, which contains **zinc**, has been studied for its effectiveness in reducing the duration of colds when taken at the first sign of symptoms.  

#### **Effectiveness Based on Studies:**
- Studies suggest that zinc (like in Zicam) can **reduce the duration of cold symptoms by about 33% (1-2 days shorter)** if taken within **24 hours of symptom onset**.  
- A 2011 **Cochrane review** found that **oral zinc lozenges** (not nasal sprays) taken within the first 24 hours **may shorten colds** but noted variability in results.  
- Some studies showed Zicam's zinc formulas reduced **symptom severity** and helped speed up recovery.  

#### **Zicam - Important Notes:**
- **Zinc nasal sprays** (like older Zicam formulas) have been linked to **loss of smell (anosmia)** and are no longer recommended.  
- For best results, **take zinc lozenges (not sprays) within the first 24 hours** of symptoms appearing.

---

## Be Vigilant

### **Tracking Respiratory Infections**

- To check respiratory infection cases and deaths, search:
    - _respiratory virus public health dashboard or report {CITY}_
    - Example for Los Angeles: [LA County Respiratory Watch](http://ph.lacounty.gov/acd/respwatch/)

### **Flu and COVID Vaccination**

- If you got sick despite receiving the flu shot, remember:
    - Vaccines are not 100% effective, although it's better to take the flu shot than not.
    - Search online for flu shot effectiveness: _flu shot {YEAR} effectiveness_
    - Example (Flu.com): 2023-2024 flu vaccine was estimated to be **42% effective**.
    - The 2024-2025 flu shot is predicted to be **less than 50% effective** but reduces hospitalization risk.

### **Early Symptoms & Recovery Factors**

- A friend and I got sick at the same working space.
- He took **Zicam** and recovered in **1.5 days**, whereas I’m sick for **4+ days**.
- Later he revealed his roommate was sick before I got sick. He's having fevers like me. And he tested negative for Covid.
- **Ruled out factors:**
    - No recent COVID vaccinations, so it wasn’t that improving recovery for him.
- **Possible reasons for his faster recovery:**
    - Regular exercise (I haven’t exercised in over a year).

