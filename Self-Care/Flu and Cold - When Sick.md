## Flu vs Cold
A **cold** develops gradually with mild symptoms like a runny nose and sore throat.  
The **flu** comes on suddenly with fever, body aches, and severe fatigue.

### **When to Seek Medical Attention**
- **Fever lasting >3 days** → Urgent Care
- **Check Respiratory Symptoms:**
    - Shortness of breath?
    - Breathing shallow, faster, and using accessory chest muscles?
- **Oxygen Saturation (O2 Sat)** - Buy from Amazon if you don’t have one.
- **If unstable (e.g., chest discomfort not related to ribs/coughing)** → Get help immediately.

### **Supplies to Consider**

#### **CVS (In-store pickup):**
- **Cough Suppressant** 
	- Dextromethorphan – ID required if you look young because of teen abuse
	- Can order Amazon which may ask for your DL number to verify age on a form before checkout finishes.
- **Cough Drops** (Halls)
- **Electrolytes** (if needed)
- **Immune Shot**
- **Water Bottles**
- **VapoRub** (Apply to throat and chest)
- Multivitamin

### **Amazon (AMZ):**
- **NAC** (N-acetylcysteine – mucolytic)

### Plan for frequent trips

If you have a long flu, there could be frequent trips (after every 🏥 days) to CVS for:
- Water
- Dextromethorphan
- Halls / Lozenges

---

## Main Self Care

### **Cough Treatment**
- **Dextromethorphan**: Cough suppressant (causes drowsiness)
- **Guaifenesin**: Thins and loosens mucus to clear congestion
- **Lozenges (Halls)**: Soothes sore throat and cough
	- Caveat: Researchers noted that while menthol may initially suppress cough reflexes, repeated use in quick succession may cause the body to become insensitive to it. Even more so, excessive use of them could make a cough linger.
- **Acupressure Points for Cough Relief**: [Kaizen Health Guide](https://kaizenhealthgroup.com/top-pressure-points-for-easing-coughing-at-night-naturally/)

### **Hydration When Sick**
- **General Guideline:**
    - Men: **15 cups/day**
    - Women: **11 cups/day**
- **If drinking too much water makes you feel weak:** Take **electrolytes**


---

## **Additional Symptoms & Remedies**

### Sleep Affected by Sudden Coughing as Soon as Fall Asleep
- Is it postnasal drip? If not:
- Dextromethorphan OTC ID required. Worse case, Phenergan from doctor prescription
- Caveat too much antihistamines could cause mucus to thicken and worsen coughs

### **Diarrhea**
- Some people with the flu may experience diarrhea as a symptom.
- **Electrolytes** if needed

### **Thick Mucus & Sinus Congestion**
- Thick mucus despite dry boogers → **Not enough fluids**
- **Mucolytics to thin mucus:**
    - **NAC**: Reduces mucus viscosity.
	    - Not typically something doctors recommend for cold/flu, however it should theoretically work together with Guaifenesin. Doctors however do prescribe Mucomyst (NAC) for inhalation (not orally) as a hospital treatment. Talk to your doctor about NAC use because there are nuances with NAC: 
		    - To name a few: People allergic to acetyle cysteine cannot take NAC. NAC can worsen acid reflux. People with asthma may exacerbate bronchospasm with NAC. High doses NAC could increase PAH.
    - **Guaifenesin**: Increases hydration in the respiratory tract
    - If you're cleared by the doctor, you could take NAC with Guaifenesin together to thin the mucus more effectively.
- **Spicy Food**: Clears sinuses and improves taste perception if altered
### **Hoarse Voice from Excessive Coughing**
- **Fluids (Preferably Warm)**: Hydrates the throat (Avoid excessive caffeine)
- **Resting the Voice**: Reduces strain on vocal cords
- **Humidifier**: Moistens air (Avoid if mold-prone environment)
- **Eucalyptus/Peppermint**: Natural decongestants for throat relief
- **Additional Tips:**
    - Warm herbal teas (ginger, honey, lemon)
    - Gargle warm salt water
    - Avoid caffeine/alcohol (drying effect)
    - Throat lozenges for temporary relief
    - See a doctor if hoarseness persists **beyond 2 weeks**

### **Sore Throat Remedies**
- **Lozenges (Halls)**
- **Steam Inhalation**: Breathe in steam from boiling water (cover face with towel)
- **Warm water/tea** (Limit caffeine)
- **Honey spray**
- **Warm salt water gargles**
- **Humidifier** (if mold is not an issue)

### **Acid Reflux Worsened by Cold/Flu**
- **Avoid Trigger Foods:**
    - Greasy, spicy, dairy, acidic foods, carbonated drinks
    - Keep a **food diary** to track triggers
- **Elevate Head While Sleeping:**
    - Raise bed by **6-8 inches** (Pillows alone don’t work effectively)
- **Wait 2-3 Hours After Eating Before Lying Down**
- **Additional Tips:**
    - Eat smaller, frequent meals
    - Drink water between meals, not excessively during
    - Chew slowly and thoroughly
    - Avoid caffeine, alcohol, and mint (relax LES)
    - Consider antacids or acid reducers (consult a doctor for long-term use)

---

## Weeks long flu

### **Expectorant Strategy**
At the beginning you may be taking pills/syrups that contain Dextromethorphan (10mg) and Guaifenesin (200mg). It may contain acetaminophen/Tylenol for fever and/or chest/throat discomfort. It may be the more frequent q4 doses because you want to control the coughing that's more directly caused by the immune system. You may choose to take the combo that has Dextromethorphan only during the night because you get too drowsy from it, or you may choose to go at it day and night.

At later stages of the flu, like week 2, you may be coughing more because of the increased inflammation and mucus in the lungs. You may choose to switch from the q4h doses containing guafensin to the extended release which may be 600mg Guaifenesin or 1200mg Guaifenesin. You're not taking the medication frequently for the anti coughing effects of Dextromethorphan, but more for the Guaifenesin effect - extended release so you can cough out the mucus sooner, so you can cough less frequently through the day, aiding the lung healing and letting the throat relax more.

When taking Guaifenesin, especially at higher doses, you may find your cough is more productive (you're actually coughing up sputum etc). That's good. You'll end up with less coughs that are more effective, rather than many ineffective coughs in the day.

It’s also worth considering **hydration** as an additional mucus-thinning factor, and **steam/humidifiers** can complement guaifenesin’s effects. If post-viral cough lingers, antihistamines or inhalers might help, depending on the underlying cause (e.g., postnasal drip vs. airway sensitivity).

### Menthol Strategy
- Keep in mind cough drops may help initially. Later, the body thinks of it as an irritant. So space it out.
- VapoRub also contains Menthol so keep that in mind (the cough drops may cross sensitive with the VapoRub). You'd have to space out cough drops and vaporub simultaneously

### Salt water rinse strategy
Salt water rinse orally frequently in the first week of the flu. You may go for q2h salt water rinse.

It's pointless after week 1 when it’s in the lungs. Other than it's to comfort the throat (if your throat still very sensitive to even a raft of air).

At that point, a **salt rinse** (like saline gargles or nasal rinses) won’t do much for **lung congestion** since it doesn’t reach deeper into the respiratory tract. However, it can still be **useful for throat comfort** if irritation persists from coughing.

Early on, salt rinses help with **postnasal drip and clearing out viral debris** in the throat/nasal passages. But once the immune system fight is focused mostly on the virus that's left in the lungs (week 2 onwards), it’s mainly about **managing inflammation and mucus clearance**, which requires:

- **Hydration & steam/humidifiers** to keep mucus thin.
- **Expectorants like guaifenesin** to break up lung congestion. You should aim for q12h extended release combo's rather than q4 combo's as discussed in Expectorant Strategy.
- **Less throat strain** (letting inflammation settle).

Salt rinses could still **reduce throat discomfort**, but for lung congestion, it’s better to focus on **deep breathing, controlled coughing, and hydration** to move mucus out.

---
## Eating

### **Eating or Fasting**
- **Starving the fever is a myth** – You need **calories to fight infection**. The outdated idea came from an old belief that food increased body heat.
- Even though feeding is encouraged, it may be difficult to eat your full calories when sick because of low appetite and/or altered taste. When the sickness is over, your appetite should be back. There could be a lingering cough for weeks. Really make sure to hit your calories so your throat/lungs can recover quicker.
### **Eating and supplementing in general**
- Avoid food that increases mucous such as milk
- Lozenges/halls are not meant for chewing or biting into. you suck the juice out of it
- When you have the flu or a cold, your digestive system may slow down, leading to bloating and discomfort. The following is easier to digest food:
### **Eating Catchup?**
- Did you catch the flu weight on a calorie restriction in an effort to lose weight?
- As soon as your appetite and/or taste recovered enough, aim for proper calorie target. Your body needs a lot of energy to fight the flu (even when it became just coughing and lethargy in the late stages of week 2). The body is even willing to eat 30% of your muscles in order to do this. Calculate your TDEE online for the calorie need
- Easy to boast calories food that are also nutritious:
	- Banana
	- Olive oil straight shot
	- Bread
	- Protein shakes

### **Eating When You Have Low Appetite or Altered Taste**
- **Soup**: Best option for nourishment
    - If too weak to cook, **order from Chinese restaurants** (hot & spicy soups help)
    - **Mapo Tofu** is a good option
- **If taste is altered:**
    - Choose **spicy, salty, or umami-rich foods**
    - Focus on texture: **Crunchy, creamy, chewy** (Be mindful, if you can’t taste, textures may feel unpleasant)
- **Other Recipe Ideas:**
    - Ginger simple syrup helped counteract bitter medication taste
    - **Ginger + chicken broth + fried garlic** (add rice/noodles to make it a meal)
    - Fried rice could irritate throat but if you can tolerate it without coughing then it could be tastier if you had hot sauce
- Hard/Easy to digest:**
	- ❌ **Greasy & Fried Foods** – Hard to digest and may worsen bloating.  
	- ❌ **Dairy Products** – Some people experience increased mucus production and bloating.  
	- ❌ **Carbonated Drinks** – Can cause gas and bloating.  
	- ❌ **Cruciferous Vegetables (Broccoli, Cauliflower, Cabbage, Brussels Sprouts)** – Can lead to gas and discomfort.  
	- ❌ **Legumes (Beans, Lentils, Chickpeas, etc.)** – May cause bloating due to fermentation in the gut.  
	- ❌ **Processed & Sugary Foods** – Can weaken your immune system and slow digestion.
	- ---
	- ✅ **Warm Soups & Broths** – Easy on digestion and provide hydration.  
	- ✅ **Ginger Tea** – Helps with bloating and aids digestion.  
	- ✅ **Bananas & Apples** – Gentle on the stomach and provide energy.  
	- ✅ **Plain Rice or Toast** – Light and easy to digest.  
	- ✅ **Steamed Vegetables (Carrots, Zucchini, Squash, etc.)** – Less likely to cause bloating.  
	- ✅ **Probiotic-Rich Foods (Yogurt, Kefir, Kimchi, Sauerkraut, etc.)** – Support gut health.
	- ---
	- Drinking **warm fluids**, staying hydrated, and resting will also help your body recover faster. Avoiding greasy, heavy, and bloating foods can help ease digestion.


### **Ordering Drinks for Hydration**
- **Healthier Chinese/Taiwanese Drinks:**
    - Lemon tea: Addon with honey if drink does not come with honey. Even better if they make it warm.
    - Matcha tea (limit milk because that can increase mucus viscosity and triggers more coughs)

### **Preventing Spread if You Live with Others**
- **Frequent Handwashing**
- **Disinfect surfaces** (Viruses can live on surfaces for long periods)
- **Disinfect phone** before taking it outside (Prevents community spread)

### **Post-Recovery Cleaning**

- If you had **night sweats**, consider washing **bed linens**.