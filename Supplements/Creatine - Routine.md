

When starting creatine supplementation, the common approach involves two main phases: the **loading phase** and the **maintenance phase**.

Some athletes choose to include a washout phase where they stop taking creatine for a period, usually a few weeks, to allow their body's natural creatine production to return to normal levels. This phase is optional and not all athletes feel it's necessary. many studies suggest that continuous use of creatine without cycling off does not lead to detrimental effects and still maintains its benefits. However, if it coincides to a part of your program where you go less intense, it may benefit your wallet too to drop the creatine.

Briefly:

1. 20g/day x 7 days if stomach tolerates, recommend a day spreads 4 doses totaling 20g
2. 3g/day x 21 days or 4 weeks to 6 months

Reworded:

1. **Loading Phase**: Take 20 grams of creatine per day for 5-7 days. This is usually divided into 4 doses of 5 grams each throughout the day to optimize absorption and reduce the risk of gastrointestinal discomfort.
2. **Maintenance Phase**: 2-10 grams per day. This phase can last anywhere from **4 weeks to 6 months**. The exact duration can depend on your personal goals, such as whether you're preparing for a competition or simply maintaining general fitness.
3. **Washout Phase** (optional): After completing the maintenance phase, you might choose to stop taking creatine for a period to allow your body's natural creatine production to normalize. This phase can last from **4 weeks to 8 weeks**. Not everyone opts for a washout phase, as some prefer to continue with the maintenance dose without interruption.

The flexibility in the duration of each phase allows you to tailor your creatine supplementation to match your training cycles, competitive schedule, or personal preference. Always ensure you're monitoring your body's response to the supplement and consult with a healthcare professional to optimize your regimen safely and effectively.

Not obligated: [https://www.youtube.com/watch?v=-mmpvk3VLkc](https://www.youtube.com/watch?v=-mmpvk3VLkc)